#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTcpSocket>
#include <QMessageBox>
#include <QFile>
#include <QtSql>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    authorized = false; // Сбрасываем сессию пользователя
    server = new QTcpServer(); // Создаем сервер

    connect(server, SIGNAL (newConnection()), this, SLOT (onNewConnection())); // Привязываемся к функции на случай нового подлключения
    connect(ui->pushButton, SIGNAL (clicked()), this, SLOT (onSelectPathClicked())); // Вешаем обработчик событий на кнопку
    connect(ui->pushButton_2, SIGNAL (clicked()), this, SLOT (onToggleClicked())); // Вешаем обработчик событий на кнопку
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onNewConnection()
{
    socket = server->nextPendingConnection(); // Забираем данные о подключении
    connect(socket, SIGNAL(readyRead()), this, SLOT (onSocketReadyRead())); // Привязываемся к функции на случай запроса от клиента
    connect(socket, SIGNAL(disconnected()), this, SLOT (onSocketDisconnected())); // Привязываемся к функции на случай отключения клиента
    qDebug() << "New connection, not authorized"; // Вывод сообщения о подключении в консоль
}

void MainWindow::onSocketReadyRead()
{
    QByteArray response, request; // Сырые данные для запроса и ответа
    request = socket->readAll(); // Считываем данные с клиента

    QJsonDocument docRequest = QJsonDocument::fromJson(request); // Получаем содержимое запроса
    QJsonObject obj = docRequest.object(); // Получаем документ в виде объекта

    if (!authorized) { // Если пользователь ранее не прошел авторизацию

        if (obj.find("type")->toString() == "auth") { // Если тип запроса равен auth

            QString inputLogin = obj.find("data")->toObject().find("login")->toString(); // Считываем введенный логин
            QString inputPass = obj.find("data")->toObject().find("pass")->toString(); // Считаем введеный пароль

            qDebug() << "login: " << inputLogin << "\n pass: " << inputPass;

            QJsonDocument docResponse; // Создаем содержимое ответа
            obj = docResponse.object(); // Получаем документ в виде объекта

            QString queriedLogin, queriedPass; int queriedPerm = -1;
            QSqlQuery query;
            if (query.exec("SELECT * FROM Users WHERE Name LIKE \'" + inputLogin + '\'')) {// Делаем запрос в БД, и если он успешен

                //Выводим значения из запроса
                while (query.next())
                {
                    queriedLogin = query.value(1).toString(); // берем логин
                    queriedPass = query.value(2).toString(); // пароль
                    queriedPerm = query.value(3).toInt(); // уровень прав доступа
                }

                if (inputLogin == queriedLogin && inputPass == queriedPass ) { // Если логин и пароль совпадают
                    authorized = true; // То сессия пользователя прошла авторизацию
                    obj.insert("result", "success"); // Задаем отчет о успешной авторизации
                    obj.insert("permission", queriedPerm); // Задаем уровень прав пользователя
                    docResponse.setObject(obj); // Добавляем объект в документ
                    response = docResponse.toJson(); // Конвертируем содержимое в набор байтов
                } else { // Если что то не совпадает (логин или пароль)
                    authorized = false; // Сессия пользователя не прошла авторизацию
                    obj.insert("result", "failed"); // Задаем отчет о проваленной авторизации
                    docResponse.setObject(obj); // Добавляем объект в документ
                    response = docResponse.toJson(); // Конвертируем содержимое в набор байтов
                }
            } else { // Если пользователь не найден в БД, то
                authorized = false; // Сессия пользователя не прошла авторизацию
                obj.insert("result", "failed"); // Задаем отчет о проваленной авторизации
                docResponse.setObject(obj); // Добавляем объект в документ
                response = docResponse.toJson(); // Конвертируем содержимое в набор байтов
            }

            socket->write(response); // Отправляем ответ
        }
    } else {

        if (obj.find("type")->toString() == "changePassword") { // Если тип запроса равен changePassword
            QString login = obj.find("login")->toString();
            QString newPass = obj.find("newPass")->toString();

            QSqlQuery query;
            if (query.exec("UPDATE Users SET Password=\'" + newPass + "\' WHERE name LIKE \'" + login + "\'")) {
                response.append("success");
            } else {
                response.append("failed");
            }

            socket->write(response); // Отправляем ответ
        } else if (obj.find("type")->toString() == "addUser") { // Если тип запроса равен addUser
            QString login = obj.find("login")->toString();
            QString pass = obj.find("pass")->toString();

            qDebug() << "login: " << login << "\n pass: " << pass;
            QString permission = QString::number(obj.find("permission")->toInt());

            QSqlQuery query;
            if (query.exec("INSERT INTO Users (Name, Password, Permission) VALUES (\'" + login + "\', \'" + pass + "\', \'" + permission + "\')")) {
                response.append("success");
                qDebug() << "success";
            } else {
                response.append("failed");
                qDebug() << "failed";

            }

            socket->write(response); // Отправляем ответ
        } else if (obj.find("type")->toString() == "getData") {
            QJsonDocument docResponse; // Получаем содержимое ответа
            QJsonObject obj = docResponse.object(); // Получаем документ в виде объекта
            QJsonArray arr; // массив данных

            QSqlQuery query;
            if (query.exec("SELECT * FROM Data")) { // Делаем запрос в БД
                while (query.next())
                {
                    QJsonObject jItem; // Объект строки данных

                    jItem.insert("title", query.value(0).toString()); // заполняем название
                    jItem.insert("supplier", query.value(1).toString()); // заполняем поставщика
                    jItem.insert("reciever", query.value(2).toString()); // заполняем получателя
                    jItem.insert("keeper", query.value(3).toString()); // заполняем кладовщика

                    arr.append(jItem); // Добавляем данные в массив
                }

                obj.insert("data", arr);
                docResponse.setObject(obj);
                response = docResponse.toJson();
                socket->write(response); // Отправляем ответ
            } else {
                response.append("failed");
                qDebug() << "failed";

            }
        } else if (obj.find("type")->toString() == "updateData") {
            QSqlQuery query;
            if (query.exec("DELETE FROM Data")) { // Делаем запрос в БД
                for (auto jItem : obj.find("data")->toArray()) {
                    QString title = jItem.toObject().find("title")->toString();
                    QString supplier = jItem.toObject().find("supplier")->toString();
                    QString reciever = jItem.toObject().find("reciever")->toString();
                    QString keeper = jItem.toObject().find("keeper")->toString();

                    qDebug() << query.exec("INSERT INTO Data (name, supplier, reciever, keeper) VALUES (\'" + title + "\', \'" + supplier + "\', \'" + reciever + "\', \'" + keeper + "\')");
                }

                response.append("success");
            } else {
                response.append("failed");
                qDebug() << "failed";

            }

            socket->write(response); // Отправляем ответ

        }

    }
}

void MainWindow::onSocketDisconnected()
{
    qDebug() << "Disconnected, unauthorize"; // Выводим сообщение в консоль
    authorized = false; // Заканчиваем сессию
}

void MainWindow::onSelectPathClicked() // Нажатие на кнопку "Указать"
{
    QFileDialog dlg; // Диалог выбора файла
    dlg.open(); // Открываем диалог
    ui->lineEdit->setText(dlg.getOpenFileUrl().toLocalFile()); // Сохраняем путь в поле
}

void MainWindow::onToggleClicked() // Нажатие на кнопку "Запустить / Остановить"
{
    if (!runned) {
        db = QSqlDatabase::addDatabase("QSQLITE"); // Указываем тип БД
        db.setDatabaseName(ui->lineEdit->text()); // Указываем путь к БД

        if (!db.open()) { // Открываем БД, а если не удалось, то выводим ошибку:
            QMessageBox msg;
            msg.setText("Не удалось открыть базу данных!");
            msg.exec();
            exit(EXIT_FAILURE); // Выход из приложения
        }

        if (!server->listen(QHostAddress("127.0.0.1"), 6000)) { // Запуск сервера, а если не удалось то выводим ошибку:
            QMessageBox msg;
            msg.setText("Не удалось запустить сервер!");
            msg.exec();
            exit(EXIT_FAILURE); // Выход из приложения
        }

        runned = true;
        ui->pushButton_2->setText("Остановить");
    } else {
        server->close();
        db.close();
        runned = false;
        ui->pushButton_2->setText("Запустить");

    }
}
