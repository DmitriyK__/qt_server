#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QList>
#include <QtSql>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onNewConnection();
    void onSocketReadyRead();
    void onSocketDisconnected();

    void onSelectPathClicked();
    void onToggleClicked();

private:
    Ui::MainWindow *ui;
     QTcpServer *server;
     QTcpSocket* socket;
     QSqlDatabase db;
     bool authorized;
     bool runned;
};

#endif // MAINWINDOW_H
